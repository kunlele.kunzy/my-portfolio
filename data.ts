import { RiComputerLine } from "react-icons/ri";
import { FaServer } from "react-icons/fa";
import {
  AiOutlineAntDesign,
  AiOutlineApi,
  AiOutlineMobile,
} from "react-icons/ai";
import { MdDeveloperMode } from "react-icons/md";
import { BsCircleFill } from "react-icons/bs";
import { IService, ISkill, IProject } from "./types";

export const services: IService[] = [
  {
    id: 1,
    title: "Frontend Web Development",
    about:
      "I build beautiful and scalable SPA using <b>HTML</b>, <b>CSS</b>, <b>React js</b> and <b>Nextjs</b>",
    Icon: RiComputerLine,
  },
  // {
  //   id: 2,
  //   title: "Backend Development",
  //   about:
  //     "Handle database, server, api using <b>Node js</b> and <b>Expressjs</b>",
  //   Icon: FaServer,
  // },
  {
    id: 3,
    title: "Frontend Mobile Development",
    about:
      "I build beautiful and scalable cross platform (hybrid) websites and web applications, using either <b>React Js</b> or <b>Html</b>",
    Icon: AiOutlineMobile,
  },
  // {
  //   id: 4,
  //   title: "API Development",
  //   about:
  //     "I develop robust REST and GraphQL APIs with <b>Node js</b>, <b>Expressjs</b>, <b>Loopbackjs</b> and <b>Nextjs</b>",
  //   Icon: AiOutlineApi,
  // },
  {
    id: 5,
    title: "Tech Wiz Metacoders team participant",
    about: "A global tech based competition conducted  <b>for website design </b> and <b>and web application</b>",
    Icon: MdDeveloperMode,
  },
  
];

export const languages: ISkill[] = [
  {
    name: "JavaScript",
    level: "90%",
    Icon: BsCircleFill,
  },
  {
    name: "React",
    level: "80%",
    Icon: BsCircleFill,
  },
  // {
  //   name: "React Native",
  //   level: "80%",
  //   Icon: BsCircleFill,
  // },
  // {
  //   name: "Dart",
  //   level: "65%",
  //   Icon: BsCircleFill,
  // },
  {
    name: "Python",
    level: "50%",
    Icon: BsCircleFill,
  },
  {
    name: "TypeScript",
    level: "40%",
    Icon: BsCircleFill,
  },
  {
    name: "Nodejs",
    level: "50%",
    Icon: BsCircleFill,
  },
 
];

export const tools: ISkill[] = [
  { name: "Git", level: "80%", Icon: BsCircleFill },
  { name: "Click Up", level: "80%", Icon: BsCircleFill },
  { name: "Zoom", level: "90%", Icon: BsCircleFill },
  { name: "Google Meet", level: "99%", Icon: BsCircleFill },
  { name: "Figma", level: "45%", Icon: BsCircleFill },
  { name: "VSCode", level: "75%", Icon: BsCircleFill },
];

export const projects: IProject[] = [
  {
    id: 1,
    name: "COVID Tracker",
    description:
      "This web app show statistics of the Corona virus both globally and can be filtered to a specific country, it shows the death, infected and recovered count and plot them in a chart as well",
    category: ["react"],
    image_path: "/images/covid-tracker.png",
    github_url: "https://gitlab.com/kunlele.kunzy/covid-tracker.git",
    // deployed_url: "https://devxandercode.github.io/covid-19-tracker/",
    key_techs: ["React", "Chartjs", "Material UI"],
  },
  {
    id: 2,
    name: "Stripe",
    description:
      "A Full payment gateway",
    category: ["node"],
    image_path: "/images/checkout.png",
    github_url: "https://gitlab.com/kunlele.kunzy/stripe-checkout.git",
    // deployed_url: "https://my-portfolio-website-react.herokuapp.com/",
    key_techs: ["Javascript", "Node"],
  },
  {
    id: 3,
    name: "Ecommerce website",
    description:
      "A basic ecommerce website that displays a list of products with the add to cart functionality",
    category: ["react"],
    image_path: "/images/ecommerce.png",
    github_url: "https://gitlab.com/kunlele.kunzy/react-e-commerce-store.git",
    deployed_url: "https://e-commerce-store-phone.netlify.app",
    key_techs: ["React", "Styled Component", "Context Api", "Bootstrap"],
  },
  // {
  //   id: 4,
  //   name: "Weather App",
  //   description:
  //     "A basic react weather app that basically tells you the weather and temperature of any city and literally change the background accordingly",
  //   category: ["react"],
  //   image_path: "/images/weather.png",
  //   github_url: "https://github.com/DevXanderCode/react-weather-app",
  //   deployed_url: "https://weather-x3-app.netlify.app/",
  //   key_techs: ["React", "Axios", "Open weather map"],
  // },
  // {
  //   id: 6,
  //   name: "Facebook UI",
  //   description:
  //     "A web and mobile app built with flutter with a facebook like user interface",
  //   category: ["flutter"],
  //   image_path: "/images/facebook.jpeg",
  //   github_url: "https://github.com/DevXanderCode/facebook_ui_clone",
  //   // deployed_url: "https://alex-portfolio-nu.vercel.app/",
  //   key_techs: ["Dart", "Cached Network Image", "Flutter"],
  // },
 
  {
    id: 9,
    name: "Portfolio website",
    description:
      "My Portofolio website that shows my experience, a bio about me and also a list of some of my projects",
    category: ["react", "nextjs", "typescript"],
    image_path: "/images/portfolio.png",
    github_url: "https://gitlab.com/kunlele.kunzy/my-portfolio.git",

    // deployed_url: "https://alex-portfolio-nu.vercel.app/",
    key_techs: [
      "React",
      "Nextjs",
      "TypeScript",
      "Framer Motion",
      "TailwindCss",
    ],
  },
];
